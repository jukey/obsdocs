# obsdocs

Personal collection of OBS related guides and tools.

## How to install OBS and make it a virtual cam

This guide expains
1. How to install OBS on Ubuntu
2. How to install a virtual cam to be used for Video Calls

### 1. How to install OBS on Ubuntu

OBS is available as snap or from the default repositories. I prefer the one I 
get with `apt` because it has a better startup time and I never run into 
problems when editing the config. The snap version sometimes crashes, when
editing the configuration.

Following [this guide](https://srcco.de/posts/using-obs-studio-with-v4l2-for-google-hangouts-meet.html) 
I installed OBS.

```bash
sudo apt install obs-studio
```

#### 2. How to install the virtual cam

Installing a virtual cam allows you to stream the arrangements you did in OBS.
E.g. you can add graphical overlays to the picture your webcam sends. Or you 
can share a specific window or desktop.

In addition to the steps mentioned in the guide I needed to also install the
following package:
```bash
sudo apt install libobs-dev 
```

```bash
sudo apt install v4l2loopback-dkms
sudo modprobe v4l2loopback devices=1 video_nr=10 card_label="OBS Cam" exclusive_caps=1
sudo apt install qtbase5-dev
git clone --recursive https://github.com/obsproject/obs-studio.git
git clone https://github.com/CatxFish/obs-v4l2sink.git
cd obs-v4l2sink
mkdir build && cd build
cmake -DLIBOBS_INCLUDE_DIR="../../obs-studio/libobs" -DCMAKE_INSTALL_PREFIX=/usr ..
make -j4
sudo make install
```

In my case the OBS version I installed, put their config into the wrong folder
so we need to copy it over:

```bash
cp /usr/lib/obs-plugins/v4l2sink.so ~/.config/obs-studio/plugins/v4l2sink/bin/64bit

```

That's it. Happy video calling!